# A minimal simple reverse PHP proxy

```mermaid
graph TD;
    A-->B;
    B-->A;
    B-->C;
    C-->B;
``````

* `A` is your website visitor / Postman / etc.
* `B` is this reverse proxy
* `C` is the target / proxied, for example https://example.com

All requests (POST, GET, PUT, etc) go to `B` (this reverse proxy) and relayed to `C` (target/proxied server).

## How to test/run

`php -S localhost:8084 -t public/`

Use Postman and test it against http://localhost:8084

\* the port does not have to be 8084, it can be any number
